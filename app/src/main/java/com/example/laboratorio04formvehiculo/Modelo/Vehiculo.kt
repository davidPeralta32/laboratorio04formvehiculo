package com.example.laboratorio04formvehiculo.Modelo

import android.widget.Toast
import com.example.laboratorio04formvehiculo.MainActivity

class Vehiculo (val nombre:String, var marca:String, val anio : String, var Descripcion :String){

    fun datosVehiculo():String{
       return """
            DATOS VEHICULO
            Titular : $nombre
            Marca : $marca
            Año : $anio
            Descripcion : $Descripcion
        """.trimIndent()
    }
}