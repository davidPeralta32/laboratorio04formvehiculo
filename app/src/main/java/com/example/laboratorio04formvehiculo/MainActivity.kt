package com.example.laboratorio04formvehiculo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Toast
import com.example.laboratorio04formvehiculo.Modelo.Vehiculo
import com.example.laboratorio04formvehiculo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), AdapterView.OnItemClickListener {

    private lateinit var binding : ActivityMainBinding
    private var item : String? = null
    private val anioCurso : Int = 2021

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val marcas : List<String> = listOf("Ford","Nissan","Chevrolet","Audi")
        val adapter = ArrayAdapter(this,R.layout.list_item,marcas)

        with(binding.cmbModelo){
            setAdapter(adapter)
            onItemClickListener = this@MainActivity
        }

        //event clic boton
            binding.btnConfirmar.setOnClickListener {
            //Obtener valores de la interfaz XML y guardar variable
            val titular = binding.etTitular.text.toString()
            val marca = item?: ""
            val anio = binding.etAnio.text.toString()
            val descripcion = binding.etDescripcion.text.toString()

            //Validar datos
            if (titular.isEmpty()){
                mensajeToast("agregar el Titular")
                return@setOnClickListener
            }
            if (marca.isEmpty()){
                mensajeToast("Agregar la Marca")
                return@setOnClickListener
            }
            if (anio.isEmpty()){
                mensajeToast("Agregar el Año")
                return@setOnClickListener
            }
            if (anio.toInt() > anioCurso){
                mensajeToast("año no valido")
                return@setOnClickListener
            }
            if (descripcion.isEmpty()){
                mensajeToast("Agregar una Descripcion")
                return@setOnClickListener
            }

            val vehiculo1 = Vehiculo(titular,marca,anio,descripcion)
            val mensaje = vehiculo1.datosVehiculo()
            mensajeToast("$mensaje")
            limpiarEdit()

        }


    }

    fun limpiarEdit(){
        binding.etTitular.setText("")
        binding.etAnio.setText("")
        binding.etDescripcion.setText("")
        binding.cmbModelo.setText("")
    }

    fun mensajeToast(Mensaje:String){
        Toast.makeText(this,"$Mensaje",Toast.LENGTH_SHORT).show()
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        item = parent?.getItemAtPosition(position).toString()

    }
}